FROM alpine

WORKDIR /var/md5er

RUN apk add --no-cache parallel && mkdir ~/.parallel && touch ~/.parallel/will-cite

COPY md5er.sh ./
RUN chmod +x md5er.sh

ENTRYPOINT ["./md5er.sh"]

