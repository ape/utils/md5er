#!/bin/sh

# output csv header
echo 'md5sum'

# Skip input csv header, echo md5sum for each line
tail -n +2 /dev/stdin | parallel 'echo {} | grep -oE "^[^,;]+" | tr -d "\n\"" | md5sum | grep -oE ^\\w+'

