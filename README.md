# MD5er

This utility takes a csv file in, computes the md5sum of the first column
for each line and outputs a new one-column csv file with md5sums.

## Input Format

- The first row MUST contains headers.
- Columns are separated by commas (`,`) or semicolons (`;`).
- Values may be quoted with double quotes (`"`).

### Notices

- Only values in the **first column** are computed.
- Double quotes are automatically detected and stripped before computation.

## Output Format

1. The first row is a header.
2. The sole column represents unquoted md5sum.

## Setup

```bash
docker build -t ape-md5er 'https://framagit.org/ape/utils/md5er.git#latest'
```

## Usage

```bash
docker run --rm -i ape-md5er <~/input.csv >~/output.csv
```

